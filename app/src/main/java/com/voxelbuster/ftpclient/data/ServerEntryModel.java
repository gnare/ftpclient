package com.voxelbuster.ftpclient.data;

import java.util.UUID;

public class ServerEntryModel {
    private final String name, host;
    private final int port;

    private final UUID uuid;

    private final boolean pasv, askPass;
    private final int protocol;
    private final String user, pass;

    public ServerEntryModel(String name, String host, int port, boolean pasv, boolean askPass, int protocol, String user, String pass) {
        this.name = name;
        this.host = host;
        this.port = port;
        this.pasv = pasv;
        this.askPass = askPass;
        this.protocol = protocol;
        this.user = user;
        this.pass = pass;
        this.uuid = UUID.randomUUID();
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getName() {
        return name;
    }

    public boolean isPasv() {
        return pasv;
    }

    public int getProtocol() {
        return protocol;
    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }

    public boolean doesAskForPassword() {
        return askPass;
    }

    public UUID getUuid() {
        return uuid;
    }
}
