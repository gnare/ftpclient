package com.voxelbuster.ftpclient.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class CommonDialog {
    public static AlertDialog createYesNo(Context c, String title, String message, DialogInterface.OnClickListener yesListener, DialogInterface.OnClickListener noListener) {
        return new AlertDialog.Builder(c)
                .setTitle(title)
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, yesListener)
                .setNegativeButton(android.R.string.no, noListener)
                .create();
    }
}
